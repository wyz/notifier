require 'gserver'
require 'drb'
require_relative "../config/drb_config"

trap("INT") do
  puts "bye!"
  exit
end

class EventSourceServer < GServer
  attr_reader :master_server

  def initialize(*)
    @master_server = DRbObject.new_with_uri(DRB_SERVER_URI)
    super
  end

  def serve(io)
    loop do
      event = io.readline
      send_message(event)
    end
  end

  def start(*)
    DRb.start_service
    super
  end

  def keep_alive
    loop {}
  end

  private
  def send_message(event)
    master_server.push_event(event)
  end
end