class Event
  attr_accessor :user_id, :message, :sequence

  def initialize(seq, user_id, message)
    @user_id = user_id
    @message = message
    @sequence = seq
  end
end