require_relative "event"
class EventBuilder

  def initialize(event_message)
    @events = self.class.split(event_message)
  end

  def events
    Array(@events)
  end

  def self.split(event_message)
    sequence, type, from_user_id, to_user_id = event_message.split("|").map(&:chomp)
    User.new(from_user_id) if from_user_id
    User.new(to_user_id) if to_user_id

    case type
    when "F"
      User.find(to_user_id).followers.concat Array(from_user_id)
      Event.new(sequence, to_user_id, event_message)
    when "B"
      Event.new(sequence, nil, event_message)
    when "P"
      Event.new(sequence, to_user_id, event_message)
    when "S"
      user = User.find(from_user_id)
      user.followers.map{|user_id| Event.new(sequence, user_id, event_message)}
    else
      []
    end
  end
end