class User
  attr_accessor :user_id, :followers
  
  def self.new(user_id)
    @cache ||= {}
    @cache[user_id] ||= super(user_id)
  end

  def self.find(user_id)
    self.new(user_id)
  end

  def initialize(user_id)
    @user_id = user_id
    @followers = []
  end
end