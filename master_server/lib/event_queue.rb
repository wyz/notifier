class EventQueue
  attr_accessor :queue
  def initialize(queue = [])
    @queue = queue
    @sorted = false
  end

  def enqueue(events)
    events.each do |event|
      queue << [event.sequence.to_i, event]
    end
  end

  def order!
    queue.sort_by!{|e| e[0]} unless @sorted
    @sorted = true
  end

  def next
    queue.shift
  end
end