require 'drb/drb'
require_relative "lib/user"
require_relative "lib/event_queue"
require_relative "lib/event_builder"

trap("INT") do
  puts "bye!"
  exit
end

class Server
  SECONDS_FOR_READY_STATE = 1

  attr_reader :event_queue, :ready, :last_event_at

  def initialize
    @event_queue = EventQueue.new
    @ready = false
    @counter = 0
  end

  def push_event(event)
    builder = EventBuilder.new(event)
    events = builder.events
    @last_event_at = Time.now
    event_queue.enqueue events if events.any?
    return true
  end

  def get_notification
    if ready
      event_queue.order!
      event = event_queue.next
      if event
        {user_id: event[1].user_id, message: event[1].message}
      end
    else
      set_ready
    end
  end

  private
  def set_ready
    @ready = true and return if last_event_at && Time.now > last_event_at + SECONDS_FOR_READY_STATE
  end
end

