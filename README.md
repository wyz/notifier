# Challenge

## How to run

* execute `start_master_server`
* execute `start_user_server`
* execute `start_event_source_server`
* execute `follower_maze.sh`

## How it works

####Observations
* The system **does not care for the number of events** it receives. It starts notifying the users *1 second* after it stopped receiving any events
* Each of **the folders containing servers** are in fact meant to be projects on their own. They can be created quite independently but for the sake of simplicity i did not create 3 repositories.

The event source is connecting on **event_source server**. This server in turn, is sending it's payload to the **master server**.

The way this is achieved is having the **master server** exposing an object through a **DRb connection**. The event source server is calling directly methods on the object through the DRb connection.

After the event source has sent it's payload, the **user server** is connecting to the same Drb server to ask for data to send to the users.

I chose this architecture because this way the 2 servers to which the Scala program is trying to connect are fairly thin. They only have to know about **2 endpoints** on master server. In such a way, all the complexity can stay on the master server.