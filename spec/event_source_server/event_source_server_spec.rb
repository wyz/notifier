require_relative "../spec_helper"
require_relative "../../event_source_server/event_source_server"
require 'socket'

describe EventSourceServer do
  let(:server) { described_class.new(10101) }

  before do
    DRbObject.stub(:new_with_uri).and_return(stub)
    server.start
  end

  after do
    server.shutdown
  end

  it "should send a message to master_server" do
    TCPSocket.open "localhost", 10101 do |socket|
      message = "hi there!"
      server.master_server.should_receive(:push_event).once.with(message)

      socket.print message
    end
  end
end