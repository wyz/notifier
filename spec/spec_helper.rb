irresponsible = -> { system("open", "http://twitter.com/ariejan/status/350936170923950082/photo/1") }
colored_text = ->(color, text) { "\e[#{color}m#{text}\e[0m" }
red_text = colored_text.curry[32]

RSpec.configure do |config|
  config.after(:suite) do
    if RbConfig::CONFIG['host_os'] =~ /darwin/ && ARGV.empty?
      irresponsible.call
      puts red_text.call("run 'rspec .' to get rid of web-page")
    end
  end
end