describe "the whole system" do
  it "should work" do
    begin
      system "mkdir tmp"
      system("start_master_server &")
      system("start_event_source_server &")
      system("start_user_server &")
      system("followermaze.sh > tmp/out.tmp 2>&1 && exit")
      system("pkill -f _server")

      File.open("tmp/out.tmp", "r").read.should match(/ALL NOTIFICATIONS RECEIVED/)
    ensure
      system("pkill -f _server")
      system("rm -rf tmp")
    end
  end
end