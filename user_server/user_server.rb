require 'gserver'
require 'drb'
require_relative "../config/drb_config"

trap("INT") do
  puts "bye!"
  exit
end

class UserServer < GServer
  attr_reader :master_server
  attr_accessor :user_connections

  def initialize(*)
    @master_server = DRbObject.new_with_uri(DRB_SERVER_URI)
    @user_connections = Hash.new
    super
  end

  def serve(io)
    loop do
      user_id = io.readline.chomp
      user_connections[user_id] = io
    end
  end

  def start(*)
    DRb.start_service
    super
  end

  def listen_for_notifications
    loop do
      next unless (event = get_event)
      user_id, message = split_event(event)

      if user_id
        io = user_connections[user_id]
        io.puts(message) if io
      else
        io_connections = user_connections.values
        io_connections.each do |io|
          io.puts(message)
        end
      end
    end
  end

  private

  def split_event(event)
    user_id = event.fetch(:user_id) { "User id or nil needed!" }
    message = event[:message]

    return user_id, message
  end

  def get_event
    master_server.get_notification
  rescue DRb::DRbConnError
    nil
  end
end





